unit uirc;

interface
uses
  SysUtils,
  synsock, blcksock, synautil;

const
  cIrcProtocol = '6667';

type
  TIRCSend = class (TSynaClient)
  private
    FSock: TTCPBlockSocket;
    function Connect: Boolean;
  public
    constructor Create;
    destructor Destroy; override;

    function Login: Boolean;
    function Logout: Boolean;
    function ReadResult: Integer;

  end;

implementation

{ TIRCSend }

function TIRCSend.Connect: Boolean;
begin
  FSock.CloseSocket;
  FSock.Bind(FIPInterface, cAnyPort);
  if FSock.LastError = 0 then
    FSock.Connect(FTargetHost, FTargetPort);
//  if FSock.LastError = 0 then
//    if FFullSSL then
//      FSock.SSLDoConnect;
  Result := FSock.LastError = 0;
end;

constructor TIRCSend.Create;
begin
  inherited Create;
  FSock := TTCPBlockSocket.Create;
  FSock.Owner := self;
  FSock.ConvertLineEnd := true;
  FTargetPort := cIrcProtocol;
  //FSystemName := FSock.LocalName;
  FTimeout := 300;
end;

destructor TIRCSend.Destroy;
begin
  FSock.Free;
  inherited Destroy;
end;

function TIRCSend.Login: Boolean;
begin
  if not Connect then
    Exit;
  if not ReadResult <> 220 then
    Exit;
end;

function TIRCSend.Logout: Boolean;
begin

end;

function TIRCSend.ReadResult: Integer;
var
  s: String;
begin
  Result := 0;
  FFullResult.Clear;
  repeat
    s := FSock.RecvString(FTimeout);
    FResultString := s;
    FFullResult.Add(s);
    if FSock.LastError <> 0 then
      Break;
  until Pos('-', s) <> 4;
  s := FFullResult[0];
  if Length(s) >= 3 then
    Result := StrToIntDef(Copy(s, 1, 3), 0);
  FResultCode := Result;
  EnhancedCode(s);
end;

end.
